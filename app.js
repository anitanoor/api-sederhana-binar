const http = require('http')
const fs = require('fs');

const onRequest = (request, response) => {
    console.log(request.url, request.method);
    
    let path = './view/';

    switch(request.url){
        case'/':
            path+= 'index.html'
            response.statusCode = 200;
            break;
        case'/about':
            path+= '/about.html'
            response.statusCode = 200;
            break;
        case'/about-me':
            response.statusCode = 301;
            response.setHeader('Location', '/about-me.html');
            return response.end();
        default:
            path = path + '404.html'
            response.statusCode = 404;
    }


    //Set header content type
    response.setHeader("Content-Type","text/html")

    //Send an html file
    fs.readFile(path, (err, data)=>{
        if(err){
            response.writeHead(404)
            response.write("File not found")
        }else{
            response.write(data)
        }
            
        response.end()
    })




}

http.createServer(onRequest).listen(5000,'localhost',()=>{
    console.log('Listening for request on port 5000');
})



